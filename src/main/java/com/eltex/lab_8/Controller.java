package com.eltex.lab_8;


import com.eltex.lab_8.entities.Order;
import com.eltex.lab_8.service.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class Controller {

    private static final Logger logger = LogManager.getLogger(Controller.class.getName());
    private final OrderService orderService;

    @Autowired
    public Controller(OrderService orderService) {
        this.orderService = orderService;
    }


    @RequestMapping(value = "/", params = {"command"})
    public ResponseEntity showAll(@RequestParam(value="command") String command)
            throws WrongCommandException{
        if ("readall".equalsIgnoreCase(command)){

            logger.info("Client has obtained list of orders.");
            return new ResponseEntity(orderService.showAll(), HttpStatus.OK);

        }else{

            logger.warn("Wrong command!");
            throw new WrongCommandException();

        }

    }


    @RequestMapping(value = "/", params = {"command", "id"})
    public ResponseEntity doSmthWithId(@RequestParam(value="command") String command,
                                       @RequestParam(value = "id") String id)
            throws WrongCommandException{


        UUID uuid = null;

        uuid = UUID.fromString(id);
        logger.info("Id was entered correctly.");


        if("addToCard".equalsIgnoreCase(command)){
            if(orderService.addById(uuid) == true) {
                logger.info("New order was added.");
                return new ResponseEntity("Order has been added. Order's id: " + uuid.toString(),
                        HttpStatus.OK);
            }else{
                return new ResponseEntity("Entered id is already used. Try another one, please.",
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }else if("readById".equalsIgnoreCase(command)){
            Order order = orderService.showById(uuid);
            logger.info("Show order by id");
            if(order == null){
                logger.info("no order with entered id");
                return new ResponseEntity("1 - no order with entered id",
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity(order, HttpStatus.OK);
        }else if("delById".equalsIgnoreCase(command)){
            orderService.delById(uuid);
            logger.info("Delete order by id");
            return new ResponseEntity("Order with id: " + uuid.toString() +
                    "been deleted", HttpStatus.OK);
        }else{
            logger.warn("Wrong command with id.");
            throw new WrongCommandException();

        }
    }

}
