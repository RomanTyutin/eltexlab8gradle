package com.eltex.lab_8.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

/**
 * This class describes the customer
 */

@Entity
public class Credentials implements Serializable {


    @Id
    @Column(name = "client_id", columnDefinition = "BINARY(16)")
    private UUID customersId;

    @Column(name = "name")
    private String customersName;
    @Column(name = "patronymic")
    private String customersPatronymic;
    @Column(name = "surname")
    private String customersSurname;
    @Column(name = "email")
    private String customersEmail;




    public Credentials(){
        super();
        this.customersId = UUID.randomUUID();
        this.customersName = null;
        this.customersPatronymic = null;
        this.customersSurname = null;
        this.customersEmail = null;

    }




    public UUID getCustomersId() {
        return customersId;
    }

    public void setCustomersId(UUID customersId) {
        this.customersId = customersId;
    }

    public String getCustomersName() {
        return customersName;
    }

    public void setCustomersName(String customersName) {
        this.customersName = customersName;
    }

    public String getCustomersPatronymic() {
        return customersPatronymic;
    }

    public void setCustomersPatronymic(String customersPatronymic) {
        this.customersPatronymic = customersPatronymic;
    }

    public String getCustomersSurname() {
        return customersSurname;
    }

    public void setCustomersSurname(String customersSurname) {
        this.customersSurname = customersSurname;
    }

    public String getCustomersEmail() {
        return customersEmail;
    }

    public void setCustomersEmail(String customersEmail) {
        this.customersEmail = customersEmail;
    }




    public void generateRandomCustomer(){
        Random random = new Random();
        int forRandom = random.nextInt(2);
        switch(forRandom){
            case 0:
                customersName = "Ivan";
                customersSurname = "Romanov";
                customersPatronymic = "Alexeyevich";
                customersEmail = "prostoczar@mail.ru";
                break;
            case 1:
                customersName = "Sergey";
                customersSurname = "Lebedev";
                customersPatronymic = "Sergeyevich";
                customersEmail = "sls@gmail.com";
                break;
        }
    }


    @Override
    public String toString(){
        return "Customer's Name: " + customersName + "  " + customersPatronymic +
                "  " + customersSurname + "    Customer's email: " + customersEmail +
                "    Customer's Id: " + customersId;
    }

}
