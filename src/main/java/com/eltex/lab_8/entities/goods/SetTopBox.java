package com.eltex.lab_8.entities.goods;

import javax.persistence.Entity;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

@Entity
public class SetTopBox extends Equipment {

    int channels_quantity;

    public int getChannels_quantity() {
        return channels_quantity;
    }

    public void setChannels_quantity(int channels_quantity) {
        this.channels_quantity = channels_quantity;
    }

    public SetTopBox() {
        super();
        channels_quantity = 0;
    }

    SetTopBox(int channels_quantity) {
        this.channels_quantity = channels_quantity;
    }

    @Override
    public void create() {
        super.create();
        Random random = new Random();
        channels_quantity = random.nextInt(100);
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Channel's quantity: " + channels_quantity);
    }

    @Override
    public void update() {
        super.update();

        boolean channels_quantity_input;
        do {
            channels_quantity_input = false;
            try {
                System.out.println("enter quantity of channels");
                Scanner scanner = new Scanner(System.in);
                channels_quantity = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter quantity of channels as a integer, please!");
                channels_quantity_input = true;
            }
        } while (channels_quantity_input);
    }

    @Override
    public void delete() {
        super.delete();
        channels_quantity = 0;
    }

    @Override
    public String toString (){
        return super.toString() + "    Quantity of channels: " + channels_quantity;
    }

}
