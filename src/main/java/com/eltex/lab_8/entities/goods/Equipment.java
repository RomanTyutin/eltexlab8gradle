package com.eltex.lab_8.entities.goods;


import com.eltex.lab_8.entities.ShoppingCart;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;


@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@class")
@Entity
@Table(name = "Equipment")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Equipment implements ICrudAction, Serializable {

    @Id
    @Column(name = "Equipment_id", columnDefinition = "BINARY(16)")
    private UUID id;

    private int price;

    private String firm;

    private String model;

    private String name;

    private static int count = 0;


    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "shopping_cart_id",  columnDefinition = "BINARY(16)")
    private ShoppingCart shoppingCart;


    public Equipment(){
        super();
        this.id = UUID.randomUUID();
        this.price = 0;
        this.firm = null;
        this.model = null;
        this.name = null;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getCount(){ return count;}

    public static void setCount(int count) {
        Equipment.count = count;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void create() {
        Random random = new Random();
        price = random.nextInt(100000);
        int firm_rand = random.nextInt(2);
        switch (firm_rand) {
            case 0:
                firm = "Sony";
                model = "0001";
                break;
            case 1:
                firm = "Microsoft";
                model = "1000";
                break;
        }
        name = getClass().getSimpleName();
        count++;
    }

    @Override
    public void read() {

        System.out.println("ID: " + id);
        System.out.println("Price: " + price);
        System.out.println("Firm: " + firm);
        System.out.println("Model: " + model);
        System.out.println("Name: " + name);
    }

    @Override
    public void update() {

        Scanner scanner = new Scanner(System.in);

        boolean price_input;
        do {
            price_input = false;
            try {
                System.out.println("enter price");
                price = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter price as a integer, please!");
                price_input = true;
            }
        } while (price_input);

        System.out.println("enter firm");
        do {
            firm = scanner.nextLine();
        } while (firm.isEmpty());

        System.out.println("enter model");
        do{
        model = scanner.nextLine();
        } while (model.isEmpty());

        name = getClass().getSimpleName();
    }

    @Override
    public void delete() {
        price = 0;
        firm = null;
        model = null;
        name = null;
        count--;
    }

    public String toString (){
        return "Equipment's type: " + name +
                "    Produced by: " + firm +
                "    Model: " + model +
                "    Price: " + price +
                "    Id: " + id;
    }
}
