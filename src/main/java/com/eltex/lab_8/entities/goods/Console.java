package com.eltex.lab_8.entities.goods;


import javax.persistence.Entity;
import java.util.Random;
import java.util.Scanner;

@Entity
public class Console extends Equipment {

    private String generation;

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public Console() {
        super();
        generation = null;
    }

    public Console(String generation) {
        this.generation = generation;
    }

    @Override
    public void create() {
        super.create();
        Random random = new Random();
        int gen = random.nextInt(4);
        switch (gen) {
            case 0:
                generation = "First generation";
                break;
            case 1:
                generation = "Second generation";
                break;
            case 2:
                generation = "Third generation";
                break;
            case 3:
                generation = "Fourth generation";
                break;
        }
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Generation: " + generation);

    }

    @Override
    public void update() {
        super.update();
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter generation of console");
        do {
            generation = scanner.nextLine();
        } while (generation.isEmpty());
    }

    @Override
    public void delete() {
        super.delete();
        generation = null;
    }

    @Override
    public String toString (){
        return super.toString() + "    Generation: " + generation;
    }

}
