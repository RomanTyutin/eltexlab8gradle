package com.eltex.lab_8;


import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class SpringExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UnsatisfiedServletRequestParameterException.class)
    protected ResponseEntity wrongRequest(){
            Map<String, String> commands = new HashMap<String, String>(){{
            put("!", "Wrong Request!");
            put("1","Enter \"?command=readAll\" to see list of orders");
            put("2","Enter \"?command=readById&id=[id]\" to see order with desired id");
            put("3","Enter \"?command=addToCard&id=[id]\" to add order with desired id");
            put("4","Enter \"?command=delById&id=[id]\" to delete order with desired id");
        }};

        return new ResponseEntity(commands, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(WrongCommandException.class)
    protected ResponseEntity wrongCommand(){
        return new ResponseEntity("3 - Wrong Command", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity notFormatId(){
        return new ResponseEntity("Illegal id. Enter id in " +
                "\"38400000-8cf0-11bd-b23e-10b96e4ef00d\" format, please.",
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    protected ResponseEntity noSuchId(){
        return new ResponseEntity("1 - no order with entered id",
                HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
